CREATE TABLE contacto (
	codigo bigserial,
	run numeric(13),
	dv varchar(1),
	nombres varchar(80),
	apellidos varchar(80),
	correo varchar(80),
	telefono varchar(16),
	asunto varchar(250),
	primary key (codigo)
);

CREATE TABLE registro_usuario (
	codigo_usu bigserial,
	nombre varchar(80),
	apellido varchar(80),
    usuario varchar(30),
	clave varchar(30),
	correo varchar(80),
	primary key (codigo_usu)
);