const form = document.querySelector('#frm-contacto')
const nombre = form.nombre
const email  = form.email
const usuario = form.usuario
const clave = form.clave

let.errors = document.querySelector('.errors')

form.addEventListener('submit',validar)

function validar(e){
     errors.innerHTML=''
     validarNombre(e)
     validarCorreo(e)
     validarUsuario(e)
     validarClave(e) 
}

function validarNombre(e){
    if(nombre.value == '' || nombre.value == null){
        errors.valida_form.display = 'block'
        errors.innerHTML='<li>Ingresa nombre</li>'
        e.preventDefault()
    }
 }

 function validarCorreo(e){
  if(email.value == '' || email.value == null){
      errors.valida_form.display = 'block'
      errors.innerHTML='<li>Ingresa correo</li>'
      e.preventDefault()
  }else{
      if (/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(email.value)){

      }else{
        errors.valida_form.display = 'block'
        errors.innerHTML='<li>Ingresa correo valido</li>'
        e.preventDefault()
      }
  }
  }
  function validarUsuario(e){
    if(usuario.value == '' || usuario.value == null){
        errors.valida_form.display = 'block'
        errors.innerHTML='<li>Ingresa Usuario</li>'
        e.preventDefault()
    }
 }

 function validarClave(e){
    if(clave.value == '' || clave.value == null){
        errors.valida_form.display = 'block'
        errors.innerHTML='<li>Ingresa Clave</li>'
        e.preventDefault()
    }
 }




