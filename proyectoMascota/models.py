from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)        

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)   
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Contacto(models.Model):
    codigo = models.BigAutoField(primary_key=True)
    run = models.DecimalField(max_digits=13, decimal_places=0, blank=True, null=True)
    dv = models.CharField(max_length=1, blank=True, null=True)
    nombres = models.CharField(max_length=80, blank=True, null=True)
    apellidos = models.CharField(max_length=80, blank=True, null=True)  
    correo = models.CharField(max_length=80, blank=True, null=True)
    telefono = models.CharField(max_length=16, blank=True, null=True)
    asunto = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contacto'
        
class Usuario(models.Model):
    codigo_usu = models.BigAutoField(primary_key=True)
    nombre = models.CharField(max_length=80, blank=True, null=True)
    apellido = models.CharField(max_length=80, blank=True, null=True)
    usuario = models.CharField(max_length=30, blank=True, null=True)
    clave = models.CharField(max_length=30, blank=True, null=True)    
    correo = models.CharField(max_length=80, blank=True, null=True)
     

    class Meta:
        managed = False
        db_table = 'registro_usuario'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
        
        
# Create your models here.
class Producto(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=64)
    categoria = models.CharField(max_length=32)
    precio = models.IntegerField()
    
    
    def __str__(self):
        return f'{self.nombre} -> {self.precio}'

    class Meta:
        managed = False
        db_table = 'producto'
        

    

        
class Product(models.Model):
    id = models.BigAutoField(primary_key=True)
    product_name = models.CharField(max_length=150, blank=True, null=True)
    product_description = models.CharField(max_length=200, blank=True, null=True)
    brand_code = models.BigIntegerField(blank=True, null=True)
    category_code = models.BigIntegerField(blank=True, null=True)
    img_url = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product'        


class Carts(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    product = models.ForeignKey('Product', models.DO_NOTHING, db_column='product_code', blank=True, null=True)      
    quanty = models.BigIntegerField(blank=True, null=True)
    date_add = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'carts'
        
class Product_Stock(models.Model):
    id = models.BigAutoField(primary_key=True)
    quanty = models.BigIntegerField(blank=True, null=True)
    product_name = models.CharField(max_length=150, blank=True, null=True)
    product_description = models.CharField(max_length=200, blank=True, null=True)
    brand_name = models.CharField(max_length=100, blank=True, null=True)
    img_url = models.CharField(max_length=250, blank=True, null=True)
    unit_list_price = models.BigIntegerField(blank=True, null=True)
    unit_sell_price = models.BigIntegerField(blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'product_stock' 
        
class Brand(models.Model):
    id = models.BigAutoField(primary_key=True)
    brand_name = models.CharField(max_length=100, blank=True, null=True)
    brand_description = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'brand'