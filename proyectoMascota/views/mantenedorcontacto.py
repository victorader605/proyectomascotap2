from django.shortcuts import render
from proyectoMascota.models import Contacto

def load_contacto(request):
    print('mantenedorcontacto.py -> load_contacto')
    print('method ->', request.method)
    
    if request.method == 'GET':
        try:
            codigo = request.GET['codigo']
            print('codigo ->', codigo)
            #aqui borra igual que Delete
            contacto = Contacto.objects.get(pk=codigo)
            contacto.delete()
        except Exception as e:
            print(e)
        #contactos = Contacto.objects.all
       # return render(request, 'mantenedor-contacto.html', {'contactos': contactos})
    
    if request.method == 'POST':
        try:
            #lectura de formulario
            codigo = request.POST['codigo']
            nombres = request.POST['nombres']
            apellidos = request.POST['apellidos']
            correo = request.POST['correo']
            telefono = request.POST['telefono']
            asunto = request.POST['asunto']
            #Busqueda de objeto de la base de datos
            contacto = Contacto.objects.get(pk=codigo)
            #actualizando en la memoria volatil
            contacto.nombres = nombres
            contacto.apellidos = apellidos
            contacto.correo = correo
            contacto.telefono = telefono
            contacto.asunto = asunto
            #actualizado en la base de datos
            contacto.save(force_update=True)
        except Exception as e:
            print(e)
    contactos = Contacto.objects.all
    return render(request, 'mantenedor-contacto.html', {'contactos': contactos})        
