from django.shortcuts import render, HttpResponse, redirect

# Create your views here.
from proyectoMascota.views.Carrito import Carrito
from proyectoMascota.models import Producto


# recupera todo lo de la base de datos
def tienda_index(request):
    #return HttpResponse("Hola Pythonizando")
    productos = Producto.objects.all()
    return render(request, 'tienda.html', {'productos':productos})


def agregar_producto(request, producto_id):
    carrito = Carrito(request)
    producto = Producto.objects.get(id=producto_id)
    carrito.agregar(producto)
    productos = Producto.objects.all()
    return render(request,'tienda.html', {'productos':productos})

def eliminar_producto(request, producto_id):
    carrito = Carrito(request)
    producto = Producto.objects.get(id=producto_id)
    carrito.eliminar(producto)
    productos = Producto.objects.all()
    return render(request, 'tienda.html', {'productos':productos})

def restar_producto(request, producto_id):
    carrito = Carrito(request)
    producto = Producto.objects.get(id=producto_id)
    carrito.restar(producto)
    productos = Producto.objects.all()
    return render(request,'tienda.html', {'productos':productos})

def limpiar_carrito(request):
    carrito = Carrito(request)
    carrito.limpiar()
    productos = Producto.objects.all()
    return render(request,'tienda.html', {'productos':productos})