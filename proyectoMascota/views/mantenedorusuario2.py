from django.shortcuts import render
from django.contrib.auth.models import User

def load_usuario_login(request):
    print('mantenedorusuario.py -> load_usuario')
    print('method ->', request.method)
    
    if request.method == 'GET':
        try:
            id = request.GET['id']
            print('codigo ->', id)
            #aqui borra igual que Delete
            user = User.objects.get(pk=id)
            user.delete()
        except Exception as e:
            print(e)
        #contactos = Contacto.objects.all
       # return render(request, 'mantenedor-contacto.html', {'contactos': contactos})
    
    if request.method == 'POST':
        try:
            #lectura de formulario
            id = request.POST['id']
            nombre = request.POST['nombre']
            apellido = request.POST['apellido']
            usuario = request.POST['usuario']
            correo = request.POST['correo']

            #Busqueda de objeto de la base de datos
            user = User.objects.get(pk=id)
            #actualizando en la memoria volatil
            user.first_name = request.POST.get('nombre')
            user.last_name = request.POST.get('apellido')
            user.username = request.POST.get('usuario')
            user.set_password(request.POST.get('clave'))
            user.email = request.POST.get('correo')

            #actualizado en la base de datos
            user.save(force_update=True)
        except Exception as e:
            print(e)
    usuarios = User.objects.all
    return render(request, 'mantenedor-usuario-login.html', {'usuarios': usuarios})        
