from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render

def galeria_index(request):
    print('galeria')
    return render(request, 'galeria.html')