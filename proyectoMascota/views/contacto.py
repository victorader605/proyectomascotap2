from django.shortcuts import render
from proyectoMascota.models import Contacto

def contacto_index(request):
    print('contacto_index')
    return render(request, 'contacto.html')

def formulario_contacto(request):
    print('formulario_contacto')

    if request.method == 'GET':
        print('invocación por método GET')
        run = request.GET.get('run')
        print('run {0}'.format(run))

        
    elif request.method == 'POST':
        print('invocación por método POST')
        #obtener información formulario
        #almacenandola en variables
        run = request.POST.get('run')
        dv = request.POST.get('dv')
        nombres = request.POST.get('nombre')
        apellidos = request.POST.get('apellido')
        correo = request.POST.get('correo')
        telefono = request.POST.get('telefono')
        asunto = request.POST.get('pedido')
        
        contacto = Contacto()
        contacto.run = run
        contacto.dv = dv
        contacto.nombres = nombres
        contacto.apellidos = apellidos
        contacto.correo = correo
        contacto.telefono = telefono
        contacto.asunto = asunto
        contacto.save()
        
        print('run {0}'.format(run))
        print('dv {0}'.format(dv))
        print('nombres {0}'.format(nombres))
        print('apellidos {0}'.format(apellidos))
        print('correo {0}'.format(correo))
        print('telefono {0}'.format(telefono))
        print('asunto {0}'.format(asunto))

    return render(request, 'contacto.html')