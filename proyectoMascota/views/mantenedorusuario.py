from django.shortcuts import render
from proyectoMascota.models import Usuario

def load_usuario(request):
    print('mantenedorusuario.py -> load_usuario')
    print('method ->', request.method)
    
    if request.method == 'GET':
        try:
            codigo_usu = request.GET['codigo_usu']
            print('codigo ->', codigo_usu)
            #aqui borra igual que Delete
            user = Usuario.objects.get(pk=codigo_usu)
            user.delete()
        except Exception as e:
            print(e)
        #contactos = Contacto.objects.all
       # return render(request, 'mantenedor-contacto.html', {'contactos': contactos})
    
    if request.method == 'POST':
        try:
            #lectura de formulario
            codigo_usu = request.POST['codigo_usu']
            nombre = request.POST['nombre']
            apellido = request.POST['apellido']
            usuario = request.POST['usuario']
            clave = request.POST['clave']
            correo = request.POST['correo']

            #Busqueda de objeto de la base de datos
            user = Usuario.objects.get(pk=codigo_usu)
            #actualizando en la memoria volatil
            user.nombre = nombre
            user.apellido = apellido
            user.usuario = usuario
            user.clave = clave
            user.correo = correo

            #actualizado en la base de datos
            user.save(force_update=True)
        except Exception as e:
            print(e)
    usuarios = Usuario.objects.all
    return render(request, 'mantenedor-usuario.html', {'usuarios': usuarios})        
