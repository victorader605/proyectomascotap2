"""proyectoMascota URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from proyectoMascota.views.home import index
from proyectoMascota.views.galeria import galeria_index
from proyectoMascota.views.contacto import contacto_index, formulario_contacto
from proyectoMascota.views.registro import registro_index, formulario_registro
from proyectoMascota.views.mantenedorcontacto import load_contacto
from proyectoMascota.views.mantenedorusuario import load_usuario
from proyectoMascota.views.mantenedorusuario2 import load_usuario_login
from proyectoMascota.views.login import login_index
from proyectoMascota.views.logout import logout_user
from django.contrib.auth.models import Permission ,ContentType
from proyectoMascota.models import Contacto, Producto
from proyectoMascota.views import errorpage
from proyectoMascota.api.v1 import apicontacto
from proyectoMascota.views.views import tienda_index, agregar_producto, eliminar_producto, restar_producto, limpiar_carrito
from proyectoMascota.views.products import load
from proyectoMascota.views import carts


admin.site.register(Permission)
admin.site.register(ContentType)
admin.site.register(Contacto)
admin.site.register(Producto)



urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', index),
    path('', index),
    path('galeria/', galeria_index),
    path('contacto/', contacto_index),
    path('contacto/formulario', formulario_contacto),
    path('registro/', registro_index),
    path('registro/formulario', formulario_registro),
    path('mantenedor-contacto/', load_contacto),
    path('mantenedor-usuario/', load_usuario),
    path('mantenedor-usuario-login/', load_usuario_login),
    path('login/', login_index),  
    path('logout/', logout_user),
    path('error-401/', errorpage.error_401_page),
    path('error-403/', errorpage.error_403_page),
    # API REST
    path('api/v1/contacto', apicontacto.contacto),
    #path('', tienda, name="Tienda"),
    path('tienda/', tienda_index),
    path('agregar/<int:producto_id>/', agregar_producto, name="Add"),
    path('eliminar/<int:producto_id>/', eliminar_producto, name="Del"),
    path('restar/<int:producto_id>/', restar_producto, name="Sub"),
    path('limpiar/', limpiar_carrito, name="CLS"),
    path('products/', load),
  #  path('products/', load_product),
    path('carts/', carts.load)
    
    
   
]
